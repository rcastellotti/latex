
pdf:
	latexmk --xelatex --shell-escape <file>.tex
clean:
	rm -rf *.toc *.xdv *.synctex.gz *.aux *.fdb_latexmk *.fls *.log *.out
