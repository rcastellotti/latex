FROM ubuntu:groovy

LABEL maintainer="rcastellotti.dev"

RUN ln -snf /usr/share/zoneinfo/Europe/Rome /etc/localtime && echo $TZ > /etc/timezone

RUN  apt update \
	&& apt install -qy texlive texlive-xetex latexmk \
     	&& apt install -qy python3 python3-pygments curl git  \
