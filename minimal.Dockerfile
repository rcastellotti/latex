FROM ubuntu:latest

LABEL maintainer="rcastellotti.dev"

RUN apt update && apt install -qy curl libgetopt-long-descriptive-perl libfontconfig git

COPY texlive.profile /
ENV PATH=/usr/local/texlive/2020/bin/x86_64-linux:$PATH

RUN curl -O https://ctan.mirror.garr.it/mirrors/ctan/systems/texlive/tlnet/install-tl-unx.tar.gz
RUN tar -xzf install-tl-unx.tar.gz 
RUN mv install-tl-20* install-tl
RUN cd install-tl && perl install-tl -profile /texlive.profile
RUN tlmgr install xetex latexmk
